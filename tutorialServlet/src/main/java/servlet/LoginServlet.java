package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import entities.User;

public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		response.sendRedirect("index.html");
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		if ((!username.equals("") && !password.equals(""))) {
			UserDao userDao = new UserDao();
			User user = userDao.getUser(username, password);
			if (user != null) {
				if (!user.isAdmin()) {
					Cookie cookie=new Cookie("isAdmin", "false");
					response.addCookie(cookie);
					response.sendRedirect("UserServlet");
				} else {
					Cookie cookie=new Cookie("isAdmin", "true");
					response.addCookie(cookie);
					response.sendRedirect("AdminServlet");
				}
			} else {
				response.sendRedirect("index.html");
			}
		}

	}
	
}
