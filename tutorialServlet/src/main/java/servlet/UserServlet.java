package servlet;

import java.io.IOException;
import java.util.TimeZone;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.internal.ApiResponse;

import bll.CityService;
import bll.FlightService;
import bll.UserService;
import entities.City;
import entities.User;

public class UserServlet extends HttpServlet {
	
	UserService userService;
	FlightService flightService;
	CityService cityService;
	private final static String apiKey="AIzaSyAO9NuREPZNZbu6FL-3R1fAx2sS5iCDQ7I";
	
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
		Cookie cookies[]=request.getCookies();
		String message=null;
		for (Cookie c:cookies)
		{
			if (c.getName().equals("isAdmin")) {
				message=c.getValue();
				c.setValue("loggedin");
				response.addCookie(c);
			}
		}
		if (message!=null && message.equals("false")){  
			userService=new UserService();
			flightService=new FlightService();
			cityService=new CityService();
			request.setAttribute("listOfCities",cityService.getCities());
			request.setAttribute("flights", flightService.seeAllFlights());
			request.setAttribute("message", message);
			RequestDispatcher rd=request.getRequestDispatcher("userPage.jsp");
			rd.forward(request, response);
		}
		else
		{
			response.sendRedirect("login");
		}
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		City arrivalCity=cityService.getCity(request.getParameter("arrivalCity"));
		City departureCity=cityService.getCity(request.getParameter("departureCity"));
		request.setAttribute("listOfCities",cityService.getCities());
		request.setAttribute("flights", flightService.findFlightsByCities(departureCity,arrivalCity));
		RequestDispatcher rd=request.getRequestDispatcher("userPage.jsp");
		rd.forward(request, response);
	}

}
