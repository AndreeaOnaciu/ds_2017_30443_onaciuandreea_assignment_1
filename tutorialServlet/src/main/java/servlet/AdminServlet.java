package servlet;

import java.io.IOException;
import java.sql.Date;
import java.sql.Time;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bll.CityService;
import bll.FlightService;
import entities.Flight;

public class AdminServlet extends HttpServlet {

	private CityService cityService;
	private FlightService flightService;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");

		String addFlight = request.getParameter("addFlight");
		String seeFlight = request.getParameter("seeFlight");
		String editFlight = request.getParameter("editFlight");
		String deleteFlight = request.getParameter("deleteFlight");

		if (addFlight != null) {
			addFlight(request);
		} else if (seeFlight != null) {
			seeFlight(request);
		} else if (editFlight != null) {
			editFlight(request);
		} else if (deleteFlight != null) {
			deleteFlight(request);
			request.setAttribute("listOfCities", cityService.getCities());
		}

		RequestDispatcher rd = request.getRequestDispatcher("adminPage.jsp");
		rd.forward(request, response);

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		Cookie cookies[] = request.getCookies();
		String message = null;
		
		for (Cookie c : cookies) {
			if (c.getName().equals("isAdmin")) {
				message = c.getValue();
				c.setValue("loggedin");
				response.addCookie(c);
			}
		}
		
		if (message != null && message.equals("true")) {
			cityService = new CityService();
			flightService = new FlightService();
			request.setAttribute("listOfCities", cityService.getCities());

			RequestDispatcher rd = request.getRequestDispatcher("adminPage.jsp");
			rd.forward(request, response);
		} else {
			response.sendRedirect("login");
		}
	}

	private void addFlight(HttpServletRequest request) {
		
		Flight f = new Flight();
		f.setAirplaneType(request.getParameter("airplaneType"));
		f.setArrivalCity(cityService.getCity(request.getParameter("arrivalCity")));
		f.setArrivalDate(java.sql.Date.valueOf(request.getParameter("arrivalDate")));
		String hour = request.getParameter("arrivalHour").concat(":00");
		Time time = Time.valueOf(hour);
		f.setArrivalHour(time);

		f.setDepartureCity(cityService.getCity(request.getParameter("departureCity")));
		f.setDepartureDate(Date.valueOf(request.getParameter("departureDate")));
		hour = request.getParameter("departureHour").concat(":00");
		time = Time.valueOf(hour);
		f.setDepartureHour(time);

		int id = flightService.addFlight(f);
		if (id > 0) {
			request.setAttribute("addMessage", "Success:Flight added with id:" + id);
		} else {
			request.setAttribute("addMessage", "Fail: The flight has not been added.");
		}
	}

	private void deleteFlight(HttpServletRequest request) {
		
		String id = request.getParameter("flightNumber");
		Flight f = flightService.deleteFlight(Integer.valueOf(id));
		if (f == null) {
			request.setAttribute("deleteMessage", "Fail:Flight not found.");
		} else {
			request.setAttribute("deleteMessage", "Success:Flight deleted.");
		}
	}

	private void seeFlight(HttpServletRequest request) {
		
		String id = request.getParameter("flightNumber");
		Flight f = flightService.seeFlight(Integer.valueOf(id));

		if (f == null) {
			request.setAttribute("seeMessage", "Error:Flight not found.");
		} else {
			request.setAttribute("f_airplaneType", f.getAirplaneType());
			request.setAttribute("f_arrivalCity", f.getArrivalCity().getName());
			request.setAttribute("f_arrivalDate", f.getArrivalDate());
			request.setAttribute("f_arrivalHour", f.getArrivalHour());
			request.setAttribute("f_departureCity", f.getDepartureCity().getName());
			request.setAttribute("f_departureDate", f.getDepartureDate());
			request.setAttribute("f_departureHour", f.getDepartureHour());
			request.setAttribute("f_id", id);
		}
	}

	private void editFlight(HttpServletRequest request) {
		
		String id = request.getParameter("flightNumber");

		Flight f = flightService.seeFlight(Integer.valueOf(id));
		f.setAirplaneType(request.getParameter("e_airplaneType"));
		f.setArrivalCity(cityService.getCity(request.getParameter("e_arrivalCity")));
		f.setArrivalDate(java.sql.Date.valueOf(request.getParameter("e_arrivalDate")));
		String hour = request.getParameter("e_arrivalHour");
		if (hour.length()<6) hour=hour.concat(":00");
		Time time = Time.valueOf(hour);
		f.setArrivalHour(time);
		
	

		f.setDepartureCity(cityService.getCity(request.getParameter("e_departureCity")));
		f.setDepartureDate(Date.valueOf(request.getParameter("e_departureDate")));
		hour = request.getParameter("e_departureHour");
		if (hour.length()<6) hour=hour.concat(":00");
		time = Time.valueOf(hour);
		f.setDepartureHour(time);

		int idf = flightService.editFlight(f);

		if (idf > 0) {
			request.setAttribute("editMessage", "Success:Flight updated with id:" + idf+" "+hour+" ");
		} else {
			request.setAttribute("editMessage", "Fail: The flight has not been updated.");
		}

	}

}
