package dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import entities.City;
import entities.Flight;

public class FlightDao {

	private SessionFactory factory;

	public FlightDao() {
		try {
			factory = new Configuration().configure().buildSessionFactory();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
	}

	public int addFlight(Flight flight) {
		int flightId = -1;
		Session session = factory.openSession();
		Transaction tx = null;
		String message = null;
		try {
			tx = session.beginTransaction();
			flightId = (Integer) session.save(flight);
			flight.setId(flightId);
			tx.commit();
		} catch (HibernateException e) {
			message = e.getMessage();
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return flightId;
	}

	@SuppressWarnings("deprecation")
	@Transactional
	public int editFlight(Flight flight) {
		Session session = factory.openSession();

		Transaction tr = session.beginTransaction();
		Flight f = (Flight) session.get(Flight.class, flight.getId());
		f.setAirplaneType(flight.getAirplaneType());
		f.setArrivalCity(flight.getArrivalCity());
		f.setArrivalDate(flight.getArrivalDate());
		f.setArrivalHour(flight.getArrivalHour());
		f.setDepartureCity(flight.getDepartureCity());
		f.setDepartureDate(flight.getDepartureDate());
		f.setDepartureHour(flight.getDepartureHour());
		tr.commit();
		session.update(f);

		session.close();
		return f.getId();

	}

	@SuppressWarnings("unchecked")
	public List<Flight> findFlights() {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			flights = session.createQuery("FROM Flight").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}

		} finally {
			session.close();
		}
		return flights;
	}

	@SuppressWarnings("unchecked")
	public Flight findFlight(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight WHERE id = :id");
			query.setParameter("id", id);
			flights = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}

		} finally {
			session.close();
		}
		return flights != null && !flights.isEmpty() ? flights.get(0) : null;
	}

	@SuppressWarnings("unchecked")
	public Flight deleteFlight(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> students = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight WHERE id = :id");
			query.setParameter("id", id);
			students = query.list();
			if (students.isEmpty())
				return null;
			session.delete(students.get(0));
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return students != null && !students.isEmpty() ? students.get(0) : null;
	}

	@SuppressWarnings("unchecked")
	public List<Flight> findFlightByCities(City departureCity, City arrivalCity) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			Query query = session
					.createQuery("FROM Flight WHERE departureCity in (Select id from City where name= :departureCity) "
							+ "and arrivalCity in (select id from City where name = :arrivalCity)");
			query.setParameter("arrivalCity", arrivalCity.getName());
			query.setParameter("departureCity", departureCity.getName());
			flights = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}

		} finally {
			session.close();
		}
		return flights != null && !flights.isEmpty() ? flights : null;
	}

	@SuppressWarnings("unchecked")
	public List<Flight> findFlightByDepartureCity(City departureCity) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery(
					"FROM Flight WHERE departureCity in (Select id from City where name= :departureCity) ");
			query.setParameter("departureCity", departureCity.getName());
			flights = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}

		} finally {
			session.close();
		}
		return flights != null && !flights.isEmpty() ? flights : null;
	}

	@SuppressWarnings("unchecked")
	public List<Flight> findFlightByArrivalCity(City arrivalCity) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			Query query = session
					.createQuery("FROM Flight WHERE arrivalCity in (select id from City where name = :arrivalCity)");
			query.setParameter("arrivalCity", arrivalCity.getName());
			flights = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}

		} finally {
			session.close();
		}
		return flights != null && !flights.isEmpty() ? flights : null;
	}
}
