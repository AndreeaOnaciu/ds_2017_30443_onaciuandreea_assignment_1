package bll;

import java.util.Set;

import dao.UserDao;
import entities.Flight;
import entities.User;

public class UserService {
	private UserDao userDao;
	
	public Set<Flight> getFlightsOfUser(String username,String password){
		userDao=new UserDao();
		User user=userDao.getUser(username, password);
		return user.getFlights();
	}
}
