package bll;

import java.util.List;

import dao.CityDao;
import entities.City;

public class CityService {
	
	private CityDao cityDao;
	
	public CityService() {
		cityDao=new CityDao();
	}
	
	public List<City> getCities() {
		return cityDao.findCities();
	}
	
	public City getCity(String name) {
		return cityDao.findCityByName(name);
	}
}
