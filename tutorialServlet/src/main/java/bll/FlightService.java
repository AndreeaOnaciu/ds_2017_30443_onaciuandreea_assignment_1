package bll;

import java.util.List;

import dao.FlightDao;
import entities.City;
import entities.Flight;

public class FlightService {
	private FlightDao flightDao;

	public FlightService() {
		flightDao = new FlightDao();
	}

	public int addFlight(Flight f) {
		return flightDao.addFlight(f);
	}

	public Flight seeFlight(int id) {
		return flightDao.findFlight(id);
	}

	public Flight deleteFlight(int id) {
		return flightDao.deleteFlight(id);
	}

	public int editFlight(Flight f) {
		return flightDao.editFlight(f);
	}

	public List<Flight> seeAllFlights() {
		return flightDao.findFlights();
	}

	public List<Flight> findFlightsByCities(City departureCity, City arrivalCity) {
		if (departureCity == null && arrivalCity == null) {
			return flightDao.findFlights();
		} else if (departureCity == null) {
			return flightDao.findFlightByArrivalCity(arrivalCity);
		} else if (arrivalCity == null) {
			return flightDao.findFlightByDepartureCity(departureCity);
		} else {
			return flightDao.findFlightByCities(departureCity, arrivalCity);
		}
	}
}
