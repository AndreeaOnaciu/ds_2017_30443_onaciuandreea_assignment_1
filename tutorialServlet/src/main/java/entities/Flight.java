package entities;

import java.sql.Date;
import java.sql.Time;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
//@Table(name="flight")
//@Entity
public class Flight {

//	@Id
	//@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	//@Column
	private String airplaneType;
	private City departureCity;
	private City arrivalCity;	
//	@Column
	private Date departureDate;
//	@Column
	private Date arrivalDate;
	//@Column
	private Time departureHour;
	//@Column
	private Time arrivalHour;
	
	//@ManyToMany
	private Set<User> passagers=new HashSet<User>(0);
	
	public Flight(){
		
	}

	public Set<User> getPassagers() {
		return passagers;
	}
	public void setPassagers(Set<User> passagers) {
		this.passagers = passagers;
	}
	public Flight (String airplaneType, City departureCity, City arrivalCity, Date departureDate,
			Date arrivalDate, Time departureHour, Time arrivalHour) {
		super();
		this.airplaneType = airplaneType;
		this.departureCity = departureCity;
		this.arrivalCity = arrivalCity;
		this.departureDate = departureDate;
		this.arrivalDate = arrivalDate;
		this.departureHour = departureHour;
		this.arrivalHour = arrivalHour;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAirplaneType() {
		return airplaneType;
	}
	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}
	public City getDepartureCity() {
		return departureCity;
	}
	public void setDepartureCity(City departureCity) {
		this.departureCity = departureCity;
	}
	public City getArrivalCity() {
		return arrivalCity;
	}
	public void setArrivalCity(City arrivalCity) {
		this.arrivalCity = arrivalCity;
	}
	public Date getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}
	public Date getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public Time getDepartureHour() {
		return departureHour;
	}
	public void setDepartureHour(Time departureHour) {
		this.departureHour = departureHour;
	}
	public Time getArrivalHour() {
		return arrivalHour;
	}
	public void setArrivalHour(Time arrivalHour) {
		this.arrivalHour = arrivalHour;
	}	
	
}
