package entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.ManyToMany;

public class User {
	
		private int id;
	    private String username;
	    private String password;
	    private String role;
	    //@ManyToMany
	    private Set<Flight> flights=new HashSet<Flight>(0);

	    public User(int id, String username, String password, String role, Set<Flight> flights) {
			super();
			this.id = id;
			this.username = username;
			this.password = password;
			this.role = role;
			this.flights = flights;
		}

	    public User() {
	    	
	    }
	    
		public Set<Flight> getFlights() {
			return flights;
		}

		public void setFlights(Set<Flight> flights) {
			this.flights = flights;
		}

		public String getRole() {
			return role;
		}

		public int getId() {
	        return id;
	    }

	    public void setId(int id) {
	        this.id = id;
	    }

	    public boolean isAdmin() {
	        return role.equals("admin");
	    }

	    public String getUsername() {
	        return username;
	    }

	    public void setUsername(String username) {
	        this.username = username;
	    }

	    public String getPassword() {
	        return password;
	    }

	    public void setPassword(String password) {
	        this.password = password;
	    }

	    public void setRole(String role) {
	        this.role = role;
	    }
}
