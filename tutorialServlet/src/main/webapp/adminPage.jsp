<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>LogIn</title>
</head>
<body>
<h1>Hello to the admin page!</h1>
<table>
  <tr>
  	<th>
  	<p>Complete the form to add a flight:</p>
	<form action="AdminServlet" method="post" style="width:500px">
		<label>Airplane type:</label>
		<input type="text" name="airplaneType" /><br>
		<label>Departure city:</label>
	    <select style="width:100px" name="departureCity" >
			<c:forEach items="${listOfCities}" var="city">
		   		<option name="departureCity" value="${city.getName()}">${city.getName()}</option>
			</c:forEach>
		</select><br>
		<label>Departure date:</label>
		<input type="date" name="departureDate"/><br>
		<label>Departure hour:</label>
		<input type="time" name="departureHour"/><br>
		<label>Arrival city:</label>
			<select style="width:100px" name="arrivalCity">
				<c:forEach items="${listOfCities}" var="arrCity">
		   		  <option name="arrivalCity" value="${arrCity.getName()}">${arrCity.getName()}</option>
				</c:forEach>
			</select><br>
		<label>Arrival date:</label>
		<input type="date" name="arrivalDate"/><br>
		<label>Arrival hour:</label>
		<input type="time" name="arrivalHour"/><br>
		<input type="submit" name="addFlight" value="Add flight"/>
		<p>${addMessage}</p>
		</form>
	</th>
	<th>
	<form action="AdminServlet" method="post">
	<p><label>Flight number:</label>
		<input type="text" name="flightNumber" value="${f_id}"/>
		<input type="submit" name="seeFlight" value="See Flight"/><br>
		</p>
		<p>${seeMessage}</p>
		<label>Airplane type:</label>
		<input type="text" name="e_airplaneType" value="${f_airplaneType}"/><br>
		<label>Departure city:</label>
		<input type="text" name="e_departureCity" value="${f_departureCity}"/><br>
		<label>Departure date:</label>
		<input type="date" name="e_departureDate" value="${f_departureDate}"/><br>
		<label>Departure hour:</label>
		<input type="time" name="e_departureHour" value="${f_departureHour}"/><br>
		<label>Arrival city:</label>
		<input type="text" name="e_arrivalCity" value="${f_arrivalCity}"/><br>
		<label>Arrival date:</label>
		<input type="date" name="e_arrivalDate" value="${f_arrivalDate}" /><br>
		<label>Arrival hour:</label>
		<input type="time" name="e_arrivalHour" value="${f_arrivalHour}"/><br>
		<input type="submit" name="editFlight" value="Edit Flight"/>
		<input type="submit" name="deleteFlight" value="Delete Flight"/>
		<p>${editMessage}</p>
		<p>${deleteMessage}</p>
	</form>
	</th>
	</tr>
</table>

</body>
</html>