<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>LogIn</title>
</head>
<body>

<h1>Hello to the Distributed Systems Airlines page!</h1>
<form action="UserServlet" method="post">
	<label>Departure city:</label>
	<select style="width:100px" name="departureCity" >
			<c:forEach items="${listOfCities}" var="city">
		   		<option value="${city.getName()}">${city.getName()}</option>
			</c:forEach>
			<option value="none">none</option>
	</select>
	<label>Arrival city:</label>
	<select style="width:100px" name="arrivalCity" >
			<c:forEach items="${listOfCities}" var="city">
		   		<option value="${city.getName()}">${city.getName()}</option>
			</c:forEach>
			<option value="none">none</option>
	</select>
	<input type="submit" value="Search"></input>
</form>
<br/>
<br/>
<table style="width:100%">
  <tr>
    <th>Flight Number</th>
    <th>Airplane Type</th> 
    <th>Departure City</th>
    <th>Departure Date</th>
    <th>Departure Hour</th>
    <th>Arrival City</th>
    <th>Arrival Date</th>
    <th>Arrival Hour</th>
  </tr>
  <c:forEach items="${flights}" var="flight">
    <tr>
      <td>${flight.id}</td>
      <td>${flight.airplaneType}</td>
      <td>${flight.departureCity.getName()}</td>
      <td>${flight.departureDate}</td>
      <td>${flight.departureHour}</td>
      <td>${flight.arrivalCity.getName()}</td>
      <td>${flight.arrivalDate}</td>
      <td>${flight.arrivalHour}</td>
     </tr>
    </c:forEach>
</table>

	<label>Local time:</label>
	<select style="width:100px" name="cityWS" id="cityWS">
			<c:forEach items="${listOfCities}" var="city">
		   		<option value="${city.getCoordinates()}">${city.getName()}</option>
			</c:forEach>
	</select>

<button onclick="getLocalTime()">See Local Time</button>
<div id="localTime"></div>

</body>
<script>
function getLocalTime() {

var loc = document.getElementById("cityWS")//'35.731252, 139.730291' // Tokyo expressed as lat,lng tuple
var targetDate = new Date() // Current date/time of user computer
var timestamp = targetDate.getTime()/1000 + targetDate.getTimezoneOffset() * 60 // Current UTC date/time expressed as seconds since midnight, January 1, 1970 UTC
var apikey = 'AIzaSyAO9NuREPZNZbu6FL-3R1fAx2sS5iCDQ7I'
var apicall = 'https://maps.googleapis.com/maps/api/timezone/json?location=' + loc.value + '&timestamp=' + timestamp + '&key=' + apikey

var xhr = new XMLHttpRequest() // create new XMLHttpRequest2 object
xhr.open('GET', apicall) // open GET request
xhr.onload = function() {
    if (xhr.status === 200){ // if Ajax request successful
        var output = JSON.parse(xhr.responseText) // convert returned JSON string to JSON object
        console.log(output.status) 
        if (output.status == 'OK'){ 
            var offsets = output.dstOffset * 1000 + output.rawOffset * 1000 
            var localdate = new Date(timestamp * 1000 + offsets) 
            document.getElementById("localTime").innerHTML=localdate.toLocaleString();
        	console.log(localdate.toLocaleString()) 
        }
    }
    else{
        alert('Request failed.  Returned status of ' + xhr.status)
    }
}
xhr.send();// send request
}
</script>
</html>